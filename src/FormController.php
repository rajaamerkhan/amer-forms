<?php

namespace Amer\Forms;

use Amer\Forms\Models\AmerForm;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    //
    function form1()
    {

        view()->share('title', 'Form 1');
        return view('forms::form1');
    }

    function form1_submit(Request $request)
    {
        $rules = [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'subject' => 'required|min:3',
            'message' => 'required|min:10|max:999',
        ];

        $this->validate($request, $rules);

        $amer_form = new AmerForm();
        $amer_form->name = $request->input('name');
        $amer_form->email = $request->input('email');
        $amer_form->subject = $request->input('subject');
        $amer_form->message = $request->input('message');
        $amer_form->type = 1;
        $amer_form->save();

        Mail::send('forms::emails.form1-query-received', ['title' => 'Query Received'], function ($m) use ($request) {
            $m->from(config('mail.from.address'), config('mail.from.name'));
            $m->to(config('mail.from.address'), $request->name)->subject('Query Received');
        });

        return redirect()->to(url('form1'))->with('success', 'We have successfully receivd your email. We will get back to you soon.');
    }
    function form2()
    {

        view()->share('title', 'Form 2');
        return view('forms::form2');
    }

    function form2_submit(Request $request)
    {
        $rules = [
            'subject' => 'required|min:3',
            'name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:10',
            'location' => 'required|min:3',
            'message' => 'required|min:10|max:999',
        ];

        $this->validate($request, $rules);

        $amer_form = new AmerForm();
        $amer_form->name = $request->input('name');
        $amer_form->email = $request->input('email');
        $amer_form->phone = $request->input('phone');
        $amer_form->location = $request->input('location');
        $amer_form->subject = $request->input('subject');
        $amer_form->message = $request->input('message');
        $amer_form->type = 2;
        $amer_form->save();

        Mail::send('forms::emails.form2-query-received', ['title' => 'Query Received'], function ($m) use ($request) {
            $m->from(config('mail.from.address'), config('mail.from.name'));
            $m->to(config('mail.from.address'), $request->name)->subject('Query Received');
        });

        return redirect()->to(url('form1'))->with('success', 'We have successfully receivd your email. We will get back to you soon.');
    }
}
