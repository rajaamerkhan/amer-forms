<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmerFormsTable extends Migration
{
    public function up()
    {
        Schema::create('amer_forms', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->string('location')->nullable();
            $table->string('subject');
            $table->text('message');
            $table->tinyInteger('type')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('amer_forms');
    }
}