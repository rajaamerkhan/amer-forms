<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('form1', 'Amer\Forms\FormController@form1');
    Route::post('form1', 'Amer\Forms\FormController@form1_submit');
    Route::get('form2', 'Amer\Forms\FormController@form2');
    Route::post('form2', 'Amer\Forms\FormController@form2_submit');
});