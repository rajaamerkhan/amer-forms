@extends('forms::app')
@section('content')
    <div class="row">
        <div class="col-md-12 text-center">
            <h2>Advanced Contact Form</h2>
            <small>Please fill in all fields to submit your concerns/queries.</small>
        </div>
        <div class="col-md-12">
            <p>&nbsp;</p>
            @if(session()->has('success'))
                <div class="alert alert-success">{{ session()->get('success') }}</div>
            @endif
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    Error occurred. Please check fields.
                </div>
            @endif
        </div>
        <form method="POST" action="{{ url('form2') }}">
            {{ csrf_field() }}
            <div class="col-md-12 form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Subject" name="subject"
                           value="{!! old('subject')  !!}">
                    @if ($errors->has('subject'))
                        <span class="help-block">
                <strong>{{ $errors->first('subject') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Your Name" name="name"
                           value="{!! old('name')  !!}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input type="email" class="form-control" placeholder="Your Email" name="email"
                           value="{!! old('email')  !!}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Your Phone" name="phone"
                           value="{!! old('phone')  !!}">
                    @if ($errors->has('phone'))
                        <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Your Location" name="location"
                           value="{!! old('location')  !!}">
                    @if ($errors->has('location'))
                        <span class="help-block">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="col-md-12 form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                <div class="col-md-12">
                                        <textarea rows="5" class="form-control" name="message"
                                                  placeholder="Type your message here...">{!! old('message')  !!}</textarea>
                    @if ($errors->has('message'))
                        <span class="help-block">
                <strong>{{ $errors->first('message') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 form-group">
                <div class="col-md-12">
                    <input type="submit" class="btn btn-info pull-right" value="Send">
                </div>
            </div>
        </form>
    </div>
@endsection