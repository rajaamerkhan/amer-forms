<html !DOCTYPE>
<head>
    <title>{{ $title }}</title>
</head>
<body>
<table style="max-width: 600px">
    <tr>
        <td style="max-width: 200px;">From:</td>
        <td>{{ Request::input('name') }}</td>
    </tr>
    <tr>
        <td>Email:</td>
        <td>{{ Request::input('email') }}</td>
    </tr>
    <tr>
        <td>Phone:</td>
        <td>{{ Request::input('phone') }}</td>
    </tr>
    <tr>
        <td>Location:</td>
        <td>{{ Request::input('location') }}</td>
    </tr>
    <tr>
        <td>Subject:</td>
        <td>{{ Request::input('subject') }}</td>
    </tr>
    <tr>
        <td>Message:</td>
        <td>{{ Request::input('message') }}</td>
    </tr>
</table>

</body>
</html>