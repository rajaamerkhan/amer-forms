# README #

Simple Laravel Package with 2 Forms that are stored in DB upon submission and also email is sent to company's email.

### What is this repository for? ###

* It is a quick contact form for receiving queries.
* Version 1.0

### Installation ###

* Add this line to *providers* array of config/app.php: "Amer\Forms\FormServiceProvider::class,"
* Run "php artisan vendor:publish"
* Run "php artisan migrate"

That's it.

Now you can access both forms at following URLs:
example.com/form1
example.com/form2


These forms will send email to the application's default email address or any email which is provided in config/mail.php

### Functionality ###

* Form1 and Form2 once submitted, are sent to the application's default email address
* The message is saved in database in *amer_forms* table
